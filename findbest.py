#!/usr/bin/python
import os, sys
import numpy as np
import csv
import time
#PROGRESS from progress.bar import Bar
#PROGRESS from progress.bar import IncrementalBar
import json
import pandas as pd
from collections import Counter
import math

PATH = "/home/lennart/Documenten/2018-2019/Evolutionary Computing/data_output"


def findBestInFile(filename, test):
    # with open(PATH + '/' + filename, 'rb') as csvfile:
    #     reader = csv.reader(csvfile, delimiter=',', quotechar='|')

    input_file = csv.DictReader(open(PATH + '/' + filename))
    best = 0.0
    mutation = 0.0
    islands = 0
    for row in input_file:
        if float(row[test]) > best:
            best = float(row[test])
            mutation = row["Mutation"]
            islands = row["Islands"]
    return [best, mutation, islands]

def findBestFile(test):
    directory = os.fsencode(PATH)
    results = [0.0, 0.0, 0, ""]

    for file in os.listdir(directory):
        filename = os.fsdecode(file)
        if filename.endswith(".csv"): 
            resultsnew =  findBestInFile(filename, test)
            if resultsnew[0] > results[0]:
                results = resultsnew
                results.append(filename)
        else:
            continue
    return results

results = findBestFile("f_total")

print(results)