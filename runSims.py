'''
Change PATH value to the folder with the assignmentfiles.

Make sure the submission.jar is exported in the same folder as the assignmentfiles

When you want fancy loading bar, install progress by using: pip install progress
Then uncomment all lines with PROGRESS
These are ate lines [40, 41, 250, 290, 302]

Make sure to change the interval to the values you want. List is in following format:
[ start value, end value, number of steps.]
This can be done at the function extend()

Make sure to change the other variables as well at the bottom of this file.
'''





# Possible parameters:

# - amountOfIslands
# (- amountOfParents)
# - populationSize
# - epochsBeforeMigration
# - mutationProbability
# - emigrantsPerPopulation
# - migrationInterval
# - numberClusters




#!/usr/bin/python
import os, sys
import numpy as np
import csv
import time
#PROGRESS from progress.bar import Bar
#PROGRESS from progress.bar import IncrementalBar
import json
import pandas as pd
from collections import Counter
import math






# Function that starts the loops through the parameters.
#
# Argumentes =  parameters = list of parameters to be tested
#               iterations = how many runs, each run has new seed
#
# Parameters can be:    Islands     (number of islands)
#                       PopSize     (size of population)
#                       Epochs      (number of epochs before migration)
#                       Mutation    (probability of mutation)
#                       Emigrants   (emigrants per population)
#                       Interval    (interval between migrations)
#                       Parents     (number of parents)
def simulate(parameters, lijst, methods, iterations=100, evaluation="BentCigarFunction", limits=300, csvName="results"):
    # parametersExtended = []
    for item in parameters:
        lijst = extend(item, lijst)

    if csvName[-4] != ".csv":
        csvName += ".csv"

    loop(lijst, iterations, evaluation, limits, parameters, methods, csvName)
    print("FINISHED!")



# Function that removes the last two lines from the json file. As the last two lines are not in json style.
def trimOutput():
    lines = open('output.json', 'r').readlines()
    lines = lines[0:-2]
    if (lines[-2:] != ['}\n', '}\n']):
        for i in reversed(lines):
            if "Epoch_" in i:
                lines = lines[:-1]
                lines.append("}")
                break
            lines = lines[:-1]
    stop = False
    while lines[-3:] == ['{\n', '}\n', '}\n']:
        lines = lines[:-3]
        lines[-1] = "}\n"
    if lines[-3][-2] == ",":
        lines[-3] = lines[-3][:-2]
    # print(lines[-3])

    open('output2.json', 'w').writelines(lines)



# Helper function that add data from dictionary to nested lists.
#
# Arguments =   data = Dictionary with the results from the simulation
#               lists = Previous made list by earlier iterations.
#
# Returns =     Nested lists with the data from the dictionary.
def tolists(data, lists=[]):
    islands = int((len(data["Epoch_1"]) - 2) / 2 + 1)
    epochs = int(len(data))
    if lists == []:
        fitnesses = [[[] for x in range(islands)] for y in range(epochs)]
        diversities = [[[] for x in range(islands)] for y in range(epochs)]
    else:
        fitnesses = lists[0]
        diversities = lists[1]
    for key in data:
        # print(key)
        index = int(key.split("_")[1])
        for key2 in data[key]:
            if key2[0] == "f":
                if key2[-3:] == "all":
                    fitnesses[index][0].append(data[key][key2])
                else:
                    # print(index)
                    # print(key2[-1])
                    fitnesses[index][int(key2[-1])].append(data[key][key2])
            else:
                if key2[-3:] == "all":
                    diversities[index][0].append(data[key][key2])
                else:
                    diversities[index][int(key2[-1])].append(data[key][key2])
    #print(fitnesses)
    #print(data[key][key2])
    return [fitnesses, diversities]

def getParameterNumber(parameter):
    if parameter == "Islands":
        return 0
    elif parameter == "PopSize":
        return 1
    elif parameter == "Epochs":
        return 2
    elif parameter == "Mutation":
        return 3
    elif parameter == "Emigrants":
        return 4
    elif parameter == "Interval":
        return 5
    elif parameter == "Parents":
        return 6
    elif parameter == "Clusters":
        return 7


# Helper function that writes the data from the nested lists to a csv file.
#
# Arguments =   lists = nested lists with the data from the dictionary.
#               parameter/parameter2/parameter3 = The parameter(s) that were tested.
#               parameterwaarde 1/2/3 = de values of the parameter(s) that were tested.
#               header = Boolean that will add the header to the csv file when state is True.
def tocsv(lists, lijst, writer, header, parameters, parameterwaardes, final_dict={}):
    fitnesses = lists[0][-1]
    diversities = lists[1][-1]
    length = len(fitnesses[0])
    for i in range(len(fitnesses)):
        if len(fitnesses[i]) == length:
            length = len(fitnesses[i])
            # print(fitnesses[i])
            if math.isnan(np.mean(fitnesses[i])):
                sys.exit()
            fitnesses[i] = np.mean(fitnesses[i])
            # except:
            #     print(fitnesses[i])
            #     print("test")
            #     sys.exit()
            # if fitnesses[i] == math.nan:
            #     print(fitnesses[i])
        else:
            fitnesses[i] = -1

    length = len(diversities[0])
    for i in range(len(diversities)):
        if len(diversities[i]) == length:
            length = len(diversities[i])
            try:
                diversities[i] = np.mean(diversities[i])
            except:
                print("test")
                sys.exit()
            if fitnesses[i] == math.nan:
                print('test')
        else:
            diversities[i] = -1

    if header:
        titels = []
        for k in parameters:
            titels.append(k)
        titels.append("f_total")
        titels.append("b_total")
        for i in range(1, len(fitnesses)):
            titels.append("f_island " + str(i))
        for i in range(1, len(fitnesses)):
            titels.append("b_island " + str(i))

        writer.writerow(titels)

    results = []
    for k in parameters:
        results.append(parameterwaardes[getParameterNumber(k)])
    results.append(fitnesses[0])
    results.append(diversities[0])
    for i in range(1, len(fitnesses)):
        results.append(fitnesses[i])
    for i in range(1, len(fitnesses)):
        results.append(diversities[i])
    writer.writerow(results)

    # final_dict.setdefault(parameters[0],[]).append(parameterwaardes[getParameterNumber(parameters[0])])
    for k in parameters:
        final_dict.setdefault(k,[]).append(parameterwaardes[getParameterNumber(k)])
    final_dict.setdefault("f_total",[]).append(fitnesses[0])
    final_dict.setdefault("b_total",[]).append(diversities[0])
    for i in range(1, len(fitnesses)):
        final_dict.setdefault("f_island " + str(i),[]).append(fitnesses[i])
    for i in range(1, len(fitnesses)):
        final_dict.setdefault("b_island " + str(i),[]).append(diversities[i])

    return final_dict


def makeequalsize(listOfValues, size):
    while len(listOfValues) != size:
        listOfValues = [math.nan] + listOfValues
    return listOfValues


# Function that loops through the parameters and does that a couple of iterations. Results are stored in a csv file.
#
# Arguments =   lijst = List of lists with all parameter information.
#               iterations = The amount of iterations.
#               evaluation = The evaluation function.
#               limits = amount of epochs that will be run
#               parameters = The parameters that will be tested.
def loop(lijst, iterations, evaluation, limits, parameters, methods, csvName):
    with open('results0.csv', 'w') as csvfile:
        writer = csv.writer(csvfile, delimiter=',', quotechar='|')
        maxiterations = iterations
        for k in parameters:
            maxiterations *= lijst[getParameterNumber(k)][3]
#PROGRESS        bar = IncrementalBar('Processing', max=maxiterations)
        header = True
        final_dict = {}
        for p1 in np.linspace(lijst[0][1], lijst[0][2], lijst[0][3], endpoint=True):
            p1 = int(p1)
            for p2 in np.linspace(lijst[1][1], lijst[1][2], lijst[1][3], endpoint=True):
                p2 = int(p2)
                for p3 in np.linspace(lijst[2][1], lijst[2][2], lijst[2][3], endpoint=True):
                    p3 = int(p3)
                    for p4 in np.linspace(lijst[3][1], lijst[3][2], lijst[3][3], endpoint=True):
                        for p5 in np.linspace(lijst[4][1], lijst[4][2], lijst[4][3], endpoint=True):
                            p5 = int(p5)
                            for p6 in np.linspace(lijst[5][1], lijst[5][2], lijst[5][3], endpoint=True):
                                p6 = int(p6)
                                for p7 in np.linspace(lijst[6][1], lijst[6][2], lijst[6][3], endpoint=True):
                                    p7 = int(p7)
                                    for p8 in np.linspace(lijst[7][1], lijst[7][2], lijst[7][3], endpoint=True):
                                        p8 = int(p8)
                                        lists = []
                                        parameterwaardes = [p1, p2, p3, p4, p5, p6, p7, p8]
                                        # print(methods[2])
                                        for j in range(iterations):
                                            os.system("java -Dpython=true -Dlimits=" + str(limits) +
                                                        " -Dselection=" + methods[0] +
                                                        " -Dcrossover=" + methods[1] +
                                                        " -Dmutation=" + methods[2] +
                                                        " -D" + lijst[0][0] + "=" + str(p1) +
                                                        " -D" + lijst[1][0] + "=" + str(p2) +
                                                        " -D" + lijst[2][0] + "=" + str(p3) +
                                                        " -D" + lijst[3][0] + "=" + str(p4) +
                                                        " -D" + lijst[4][0] + "=" + str(p5) +
                                                        " -D" + lijst[5][0] + "=" + str(p6) +
                                                        " -D" + lijst[6][0] + "=" + str(p7) +
                                                        " -D" + lijst[7][0] + "=" + str(p8) +
                                                        " -jar " + PATH + "testrun.jar -submission=player5"
                                                        + " -evaluation=" + evaluation + " -seed=" + str(15432) + " > output.json")
                                            trimOutput()
                                            with open("output2.json", "r") as read_file:
                                                data = json.load(read_file)
                                                lists = tolists(data, lists)
#PROGRESS                                            bar.next()
                                        final_dict = tocsv(lists, lijst, writer, header, parameters, parameterwaardes, final_dict=final_dict)
                                        header = False
        length_lists = len(final_dict["f_total"])
        final_dict = {k:makeequalsize(v, length_lists) for k, v in final_dict.items()}
        df2 = pd.DataFrame.from_dict(data=final_dict)
        outdir = "./Results"
        if not os.path.exists(outdir):
            os.mkdir(outdir)
        df2.to_csv(outdir + '/' + csvName)


#PROGRESS        bar.finish()


#TODO Change PATH to where you have the assignmentfiles.
PATH = "/home/lennart/Documenten/2018-2019/Evolutionary\ Computing/assignmentfiles_2017/"


#TODO Change the intervals to the values you want to test, format like:
#TODO  [start value, end value, number of steps]
# Arguments = (Parameter = the parameter to get more information for)
#
# Returns = [parameter, lowerbound, upperbound, number of steps, type]
def extend(parameter, lijst):
    if parameter == "Islands":
        lijst[0] = ['amountOfIslands',          1 , 6 , 6, 'int'   ]
    elif parameter == "PopSize":
        lijst[1] = ['populationSize',           60, 200, 14, 'int'   ] #deze hoeft waarschijnlijk niet getest te worden.
    elif parameter == "Epochs":
        lijst[2] = ['epochsBeforeMigration',    0 , 50 , 10 , 'int'   ]
    elif parameter == "Mutation":
        lijst[3] = ['mutationProbability',      0.1 , 1  , 5, 'double']
    elif parameter == "Emigrants":
        lijst[4] = ['emigrantsPerPopulation',   1 , 10 , 9 , 'int'   ]
    elif parameter == "Interval":
        lijst[5] = ['migrationInterval',        5 , 10 , 5 , 'int'   ]
    elif parameter == "Parents":
        lijst[6] = ['amountOfParents',          1 , 10 , 10 , 'int'   ]
    elif parameter == "Clusters":
        lijst[7] = ['numberClusters',          1 , 10 , 10 , 'int'   ]
    return lijst




#TODO Change here which evaluation function you want to run.
# Possibilities: "KatsuuraEvaluation", "BentCigarFunction", "SchaffersEvaluation"
Evaluation = "SchaffersEvaluation"




#TODO Change the parameters for your test. (Max 3)
# Parameters can be:   [Islands     (number of islands),
#                       PopSize     (size of population),
#                       Epochs      (number of epochs before migration),
#                       Mutation    (probability of mutation),
#                       Emigrants   (emigrants per population),
#                       Interval    (interval between migrations),
#                       Parents     (number of parents)]
parametersToTest = ["Interval"]


#TODO Change the number of iterations.
NumberOfIterations = 10


#TODO Change the max amount of epochs.
MaxEpochs = 500

#TODO Change what methods to use.
# [selecting of parents, recombination, mutation]
#
# selecting of parents ->   roulette
#                           stochastic
#                           tournament
# recombination ->  singlePoint
#                   singleArithmetic
#                   simpleArithmetic
#                   wholeArithmetic
#                   blend
#                   simulatedBinary
# mutation ->   relativeSingleGene
#               uncorrelated1
#               uncorrelatedN
#               uniform
#               gaussian
#               gaussianSingleGene
methods = ["roulette", "singlePoint", "gaussianSingleGene"]


#TODO Set true when you want to test over multiple methods.
changeMethodsBool = True


#TODO Change the fixed parameters here.
AmountIslands = 5
PopulationSize = 100
EpochsBeforeMigration = 10
MutationProb = 0.3
EmigrantsPerMigration = 2
MigrationInterval = 5
AmountParents = 10
NumberClusters = 5

paramLijst = [['amountOfIslands',          AmountIslands , AmountIslands ,                  1 , 'int'   ],
              ['populationSize',           PopulationSize, PopulationSize,                  1 , 'int'   ],
              ['epochsBeforeMigration',    EpochsBeforeMigration , EpochsBeforeMigration ,  1 , 'int'   ],
              ['mutationProbability',      MutationProb , MutationProb  ,                   1 , 'double'],
              ['emigrantsPerPopulation',   EmigrantsPerMigration , EmigrantsPerMigration ,  1 , 'int'   ],
              ['migrationInterval',        MigrationInterval , MigrationInterval ,          1 , 'int'   ],
              ['amountOfParents',          AmountParents , AmountParents ,                  1 , 'int'   ],
              ['numberClusters',           NumberClusters , NumberClusters ,                1 , 'int'   ]]



x = np.linspace(1, 6, 6, endpoint=True)
print(x)


def changeMethods(change, methods):
    if change:
        selections = ["roulette", "stochastic", "tournament"]
        crossovers = ["singlePoint", "singleArithmetic", "simpleArithmetic", "wholeArithmetic", "blend", "simulatedBinary"]
        mutations = ["relativeSingleGene", "uncorrelated1", "uncorrelatedN","uniform", "gaussian", "gaussianSingleGene"]
        total = len(selections) * len(crossovers) * len(mutations)
        for a, sel in enumerate(selections):
            for b, cross in enumerate(crossovers):
                for c, mut in enumerate(mutations):
                    print("-------------------------------------------------------------------------------------------")
                    print("(" + str(6 * 6 * a + 6 * b + c) + " / " + str(total) + ")")
                    methods = [sel, cross, mut]
                    simulate(parametersToTest, paramLijst, methods, iterations=NumberOfIterations, limits=MaxEpochs, 
                            evaluation=Evaluation, csvName=sel + "_" + cross + "_" + mut + "_results")
    else:
        simulate(parametersToTest, paramLijst, methods, iterations=NumberOfIterations, limits=MaxEpochs, evaluation=Evaluation)


# Here the program is called.
changeMethods(changeMethodsBool, methods)