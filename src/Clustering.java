import java.util.*;

import org.vu.contest.ContestEvaluation;

import java.lang.Math;

public class Clustering {

    private ExamplePopulation totalPopulation;
    private int maxNumberOfClusters;
    public static ContestEvaluation evaluation_;

    public Clustering(AbstractPopulation[] populations, int maxNumberOfClusters,
    		ContestEvaluation evaluation_) {
    	this.evaluation_ = evaluation_;
        List<Organism> allOrganism = new ArrayList<>();
        for(int i=0; i < populations.length; i++){
        	Collections.addAll(allOrganism, populations[i].getOrganisms());
        }
    
        Organism[] organismArray = allOrganism.toArray(new Organism[allOrganism.size()]);
        this.totalPopulation = new ExamplePopulation(organismArray);
        this.maxNumberOfClusters = maxNumberOfClusters;
    }
    
    public AbstractPopulation[] run() {
        int numberOfIterations = 15;
        int populationSize = this.totalPopulation.getOrganisms().length;
//        ExamplePopulation pop = new ExamplePopulation(populationSize);
        Organism[] popOrganism = this.totalPopulation.getOrganisms();
       
        double[][] simMatrix = createSimMatrix(this.totalPopulation);

        double[][] degreeMatrix = createDegreeMatrix(simMatrix);

        double[][] laplaceMatrix = unnormalizedLaplaceMatrix(simMatrix, degreeMatrix);

        
        EigenValues eigenValues = new EigenValues(simMatrix);
        eigenValues.solve();

        int numberOfClusters = determineNumberOfClusters(eigenValues.eigenvalues);
        numberOfClusters = 5;
        List<Coordinate> coordinateList = spectralMatrix(laplaceMatrix, eigenValues, numberOfClusters);
   
        KMeans kmeans = new KMeans(numberOfClusters, numberOfIterations, populationSize);
        
        List<List<Coordinate>> clusteredCoordinates = kmeans.run(coordinateList);
        
        AbstractPopulation[] newIslandPopulations = new AbstractPopulation[clusteredCoordinates.size()];
    
        for(int cluster=0; cluster< clusteredCoordinates.size(); cluster++){
            Organism[] currentClusterOrganisms = new Organism[clusteredCoordinates.get(cluster).size()];
            for(int coord=0; coord < clusteredCoordinates.get(cluster).size(); coord++){
                currentClusterOrganisms[coord] = popOrganism[clusteredCoordinates.get(cluster).get(coord).getOrganismNumber()];
            }
            ExamplePopulation currentPopulation = new ExamplePopulation(currentClusterOrganisms);
            currentPopulation.setEvaluation(evaluation_);
            newIslandPopulations[cluster] = currentPopulation;
        }
        return newIslandPopulations;
    }

    static private double euclideanDistance(double[] org_1, double[] org_2) {
        double diff_square_sum = 0.0;
        for (int i = 0; i < org_1.length; i++) {
            diff_square_sum += (org_1[i] - org_2[i]) * (org_1[i] - org_2[i]);
        }
        return Math.sqrt(diff_square_sum);
    }

    static private double[][] createSimMatrix(ExamplePopulation population) {
        Organism[] organisms = population.getOrganisms();

        double[][] simMatrix = new double[organisms.length][organisms.length];

        for(int i=0;i<organisms.length;i++){
            for( int j=0; j<organisms.length;j++){
                simMatrix[i][j]=euclideanDistance(organisms[i].getDNA(), organisms[j].getDNA());
            }
        }
        return simMatrix;
    }

    static private double[][] createDegreeMatrix(double[][] simMatrix){
        double[][] degreeMatrix = new double[simMatrix.length][simMatrix.length];

        for(int r=0; r < simMatrix[0].length; r++){
            double rowSum = 0.0;
            for(int c=0; c < simMatrix[0].length; c++){
                rowSum += simMatrix[r][c];
            }
            degreeMatrix[r][r] = rowSum;
        }
        return degreeMatrix;
    }

    static private double[][] unnormalizedLaplaceMatrix(double[][] simMatrix, double[][] degreeMatrix){
        double[][] laplaceMatrix = new double[simMatrix.length][simMatrix.length];
        for(int r=0; r < simMatrix[0].length; r++){
            for(int c=0; c < simMatrix[0].length; c++){
                laplaceMatrix[r][c] = degreeMatrix[r][c] - simMatrix[r][c];
            }
        }
        return laplaceMatrix;
    }

    static private List<Coordinate> spectralMatrix(double[][] laplacianMatrix, EigenValues eigenvalues, int k){
        int[] ordering = listSorter(eigenvalues.eigenvalues);
        int colLength = eigenvalues.eigenvectors.length;

        List<Coordinate> coordinateList = new ArrayList<>();

        for(int row=0; row < eigenvalues.eigenvectors[0].length; row++){ //loop over rows
            double[] data = new double[k];
            for(int col=0; col < k; col++){
                int eigenvectorIndex = ordering[col];
                data[col] = eigenvalues.eigenvectors[row][eigenvectorIndex];
            }
            Coordinate newCoordinate = new Coordinate(data, row);
            coordinateList.add(newCoordinate);
        }

        return coordinateList;
    }

    public static int determineNumberOfClusters(double[] eigenValues){
        int numberOfClusters = 0;
        int maxNumberOfClusters = 20;
        Arrays.sort(eigenValues);

        double max_diff = eigenValues[1] - eigenValues[0];
        for (int i = 0; i < eigenValues.length - 1; i+=2) {
            if (eigenValues[i+1] - eigenValues[i] > max_diff){
                max_diff = eigenValues[i+1] - eigenValues[i];
                numberOfClusters = i;
            }

        }
        return Math.min(numberOfClusters, maxNumberOfClusters);
    }


    public static class EigenValues{

        double[][] matrix;
        double[] eigenvalues;
        double[][] eigenvectors;

        public EigenValues(double[][] matrix){
            this.matrix = matrix;
        }

        public void solve(){
            Matrix A = Matrix.constructWithCopy(matrix);
            A = A.transpose().times(A);
            EigenvalueDecomposition e = A.eig();

            this.eigenvectors = e.getV().getArray();
            double[][] eigenvaluesMatrix = e.getD().getArray();
            this.eigenvalues = new double[eigenvaluesMatrix.length];
            for(int i =0; i < eigenvaluesMatrix.length; i ++){
                this.eigenvalues[i] = eigenvaluesMatrix[i][i];
            }
        }
    }


    private static int[] listSorter(double[] list){
        List<Double> original_list = new ArrayList<>();
        for (double item: list) {
            original_list.add(item);
        }

        Collections.sort(original_list);
        int[] indices = new int[list.length];

        for(int i=0; i<list.length; i++){
            indices[i] = original_list.indexOf(list[i]);
        }
        return indices;
    }

    private static void printMatrix(double[][] matrix){
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();
        }
    }

    public int getMaxNumberOfClusters() {
        return maxNumberOfClusters;
    }

    public ExamplePopulation getTotalPopulation() {
        return totalPopulation;
    }
}


