import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Stream;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.minBy;
import static java.util.stream.Collectors.toList;

public class KMeans {

    private int nClusters;
    private int nIterations;
    private int populationSize;

    public KMeans(int nClusters, int nIterations, int populationSize){
        this.nClusters = nClusters;
        this.nIterations = nIterations;
        this.populationSize = populationSize;
    }

    public List run(List<Coordinate> xs) {
        Stream<Coordinate> centroids = xs.stream().limit(this.nClusters);

        for (int i = 0; i < this.nIterations; i++) {
            centroids = clusters(xs, centroids.collect(toList()))
                    .stream().map(this::average);
        }
        List<Coordinate> ps = centroids.collect(toList());

        List newList = new ArrayList(clusters(xs, ps));
        return newList;
    }

    private Collection<List<Coordinate>> clusters(List<Coordinate> xs, List<Coordinate> centroids) {
        return xs.stream().collect(groupingBy((Coordinate x) -> closest(x, centroids))).values();
    }

    private Coordinate closest(final Coordinate x, List<Coordinate> choices) {
        return choices.stream()
                .collect(minBy((y1, y2) -> dist(x, y1) <= dist(x, y2) ? -1 : 1))
                .get();
    }

    public double dist(Coordinate x, Coordinate y) {
        return x.decreaseCoordinate(y).getModulus(); }

    private Coordinate average(List<Coordinate> xs) {
        return xs.stream().reduce(Coordinate::increaseCoordinate).get().div(xs.size());
    }

    public int getnClusters() {
        return nClusters;
    }

    public int getnIterations() {
        return nIterations;
    }
}
