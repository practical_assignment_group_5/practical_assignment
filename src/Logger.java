/**
 * Helper for printing messages to console
 **/
public class Logger {	

	enum LogLevel {
		FINAL(4),			//Preferably no printing at all
		PYTHON(3),			//Prints only messages used by Python script
		ERROR(2),			//Error prints worth mentioning
		INFORMATION(1),		//Information e.g. best fitness of current population
		DEBUG(0);			//Random debug lines

		private int level;
		
		private LogLevel(int level)
		{
			this.level = level;
		}
		
		public int getLevel()
		{
			return level;
		}
	}
	
	/**
	 * Minimum LogLevel for logging the message
	 **/
	public static int logLevelThreshold = LogLevel.DEBUG.getLevel();

	/**
	 * Writes the String representation (as returned by obj.toString) of the given object to the origin stream.
	 **/
	public static void log(LogLevel level, Object obj) {
		printToOut(level, obj.toString());
	}

	public static void log(LogLevel level, String msg) {
		printToOut(level, msg);
	}

	public static void log(LogLevel level, String msg, Object... objects) {
		printToOut(level, String.format(msg, objects));
	}
	
	public static void log(LogLevel level, String msg,  Throwable throwable) {
		printToOut(level, msg);
		printToOut(level, throwable.getStackTrace().toString());
	}

	public static void log(LogLevel level, Throwable throwable) {
		printToOut(level, throwable.getStackTrace().toString());
	}

	/** 
	 * Writes the specified message to the origin stream using the PrintStream format method.
	 * @param msg A formatted string, as described on http://docs.oracle.com/javase/7/docs/api/java/util/Formatter.html
	 **/
	private static void printToOut(LogLevel level, Object msg)
	{
		if(level.getLevel() >= logLevelThreshold){
			switch(level){		
			case FINAL:
			case PYTHON:
				System.out.println(msg);
				break;
			case ERROR:
				if(msg.getClass() == Throwable.class)
					System.err.println(level.name() + "\t" + ((Throwable)msg).getStackTrace().toString());
				else
					System.err.println(level.name() + "\t" + msg);
				break;
			case INFORMATION:
			case DEBUG:
				System.out.println(level.name() + "\t" + msg);
				break;
			}
		}
	}
}
