
public class MutationImpl extends RecombinationImpl {
	public static final double MUTATION_STEP_SIZE 	= 0.05; //Start value
	
	private double boundaryCheck(AbstractPopulation pop, double value) {
		if(value < pop.lowerBound)	return pop.lowerBound;
		else if(value > pop.upperBound)	return pop.upperBound;
		
		return value;
	}

	protected double[] singleGeneRelativeMutation(AbstractPopulation pop, double[] dna) {
		double[] mutatedDNA = dna;
		// To mutate gene
		int mutationIndex = pop.rng.nextInt(mutatedDNA.length);
		// The offset which will be used to multiply the gene-value with
		//	Note: 	This is currently only a relative offset, no absolute offset. This causes values close
		//			to zero to only change a little amount...
		double mutationOffset = -AbstractPopulation.MAX_MUTATION_OFFSET + (2 * AbstractPopulation.MAX_MUTATION_OFFSET) * pop.rng.nextDouble();
		mutatedDNA[mutationIndex] = mutatedDNA[mutationIndex] + mutationOffset * mutatedDNA[mutationIndex];
		mutatedDNA[mutationIndex] = mutatedDNA[mutationIndex] + AbstractPopulation.MAX_MUTATION_OFFSET
				* (pop.lowerBound + (pop.upperBound - pop.lowerBound) * pop.rng.nextDouble());

		mutatedDNA[mutationIndex] = boundaryCheck(pop, mutatedDNA[mutationIndex]);

		return mutatedDNA;
	}

	/*
	 *
	 */
	protected double[] uncorrelatedMutation1Step(AbstractPopulation pop, double[] dna) {
		double[] mutatedDNA = dna;
		double learning_rate = 1/(dna.length); //tau
		double strategy_parameter = 1;   //needs to be specified, sigma
		double treshold_stepsize = 0.01; //epsilon
		double sigma_prime = strategy_parameter*Math.exp(learning_rate*pop.rng.nextGaussian());
		if(sigma_prime<treshold_stepsize){
			sigma_prime = treshold_stepsize;
		}
		for (int i = 0; i < mutatedDNA.length; i++) {
			double randomNum = dna[i] + sigma_prime*pop.rng.nextGaussian();
			mutatedDNA[i] = boundaryCheck(pop, randomNum);
		}
		return mutatedDNA;
	}

	/*
	 *
	 */
	protected double[] uncorrelatedMutationNStep(AbstractPopulation pop, double[] dna) {
		double[] mutatedDNA = dna;
		double learning_rate = 1/(Math.sqrt(2*Math.sqrt(dna.length))); //tau
		double learning_rate_prime = 1/(Math.sqrt(2*dna.length));
		double[] strategy_parameter = dna;
		double inital_strategy_parameter = 1; //needs to be specified, sigma
		for (int j = 0; j < strategy_parameter.length; j++) {
			strategy_parameter[j] = inital_strategy_parameter;
		}
		double treshold_stepsize = 0.01; //epsilon
		double gaussian_draw_pop = pop.rng.nextGaussian(); //Draw one Gaussian used for whole mutation
		double[] sigma_prime = dna;
		for (int i = 0; i < sigma_prime.length; i++) {
			double gaussian_draw_i = pop.rng.nextGaussian(); //Draw one Gaussian for specific i
			sigma_prime[i] = strategy_parameter[i] * Math.exp(learning_rate_prime * gaussian_draw_pop + learning_rate * gaussian_draw_i);
			if (sigma_prime[i] < treshold_stepsize) { //If the sigma prime is below a treshold, than it becomes that treshold
				sigma_prime[i] = treshold_stepsize;
			}
			mutatedDNA[i] = boundaryCheck(pop, dna[i] + sigma_prime[i] * gaussian_draw_i);
		}
		return mutatedDNA;

	}

	/*
	 *
	 */
	protected double[] uniformMutation(AbstractPopulation pop, double[] dna) {
		double[] mutatedDNA = dna;
		// Loop through dna to draw random number for each gene
		for (int i = 0; i < mutatedDNA.length; i++) {
			// Draw random number from the uniform distribution within the lower and upper bound
			double randomNum = pop.lowerBound + (pop.upperBound - pop.lowerBound) * pop.rng.nextDouble();
			mutatedDNA[i] = boundaryCheck(pop, randomNum);
		}
		return mutatedDNA;
	}

	/*
	 *
	 */
	protected double[] gaussianMutation(AbstractPopulation pop, double[] dna) {
		// Non uniform Mutation
		double[] mutatedDNA = dna;
		// Loop through dna to draw random number for each gene
		for (int i = 0; i < mutatedDNA.length; i++) {
			// Draw a number from the Gaussian distribution
			double randomNum = pop.lowerBound + (pop.upperBound - pop.lowerBound) * pop.rng.nextGaussian();
			mutatedDNA[i] = boundaryCheck(pop, randomNum);
		}
		
		return mutatedDNA;
	}
	
	/*
	 *
	 */
	protected double[] gaussianSingleGeneMutation(AbstractPopulation pop, double[] dna) {
		// Non uniform Mutation
		double[] mutatedDNA = dna;
		int mutationIndex = pop.rng.nextInt(mutatedDNA.length);
		// Draw a number from the Gaussian distribution
		double randomNum = pop.lowerBound + (pop.upperBound - pop.lowerBound) * pop.rng.nextGaussian();
		mutatedDNA[mutationIndex] = boundaryCheck(pop, randomNum);
		
		
		return mutatedDNA;
	}
	
}
