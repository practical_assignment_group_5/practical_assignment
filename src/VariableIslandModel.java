import java.util.Random;

public class VariableIslandModel {
	
	/** Default values for more readable code **/
	private static final int AMOUNT_OF_SELECTION_FUNCTIONS = 3;
	private static final int SELECTION_ROULETTE = 0;
	private static final int SELECTION_TOURNAMENT = 1;
	private static final int SELECTION_UNIFORM = 2;
	
	private static final int AMOUNT_OF_RECOMBINATION_FUNCTIONS = 6;
	private static final int RECOMBINATION_SINGLE_POINT = 0;
	private static final int RECOMBINATION_SIMPLE_ARITHMETIC = 1;
	private static final int RECOMBINATION_SINGLE_ARITHMETIC = 2;
	private static final int RECOMBINATION_WHOLE_ARITHMETIC = 3;
	private static final int RECOMBINATION_BLEND = 4;
	private static final int RECOMBINATION_SIMULATED_BINARY = 5;
	
	private static final int AMOUNT_OF_MUTATION_FUNCTIONS = 6;
	private static final int MUTATION_RELATIVE_SINGLE_GENE = 0;
	private static final int MUTATION_UNCORRELATED_1STEP = 1;
	private static final int MUTATION_UNCORRELATED_NSTEP = 2;
	private static final int MUTATION_UNIFORM = 3;
	private static final int MUTATION_GAUSSIAN = 4;
	private static final int MUTATION_GAUSSIAN_SINGLE_GENE = 5;
	
	private AbstractPopulation[] populations;
	private Random rng;

	public VariableIslandModel(int numberOfPopulation, long seed) {
		rng = new Random(seed);
		populations = new AbstractPopulation[numberOfPopulation];
		
		for(int i = 0; i < numberOfPopulation; i++) {		
			populations[i] = generatePopulation();
			populations[i].setEvaluation(player5.evaluation_);
		}
	}
	
	private AbstractPopulation generatePopulation() {
		
		AbstractPopulation randomPop = new AbstractPopulation(rng.nextLong()) {
			@Override
			Organism[] selectParents(int amountOfParents) {
				int randomIndex = rng.nextInt(AMOUNT_OF_SELECTION_FUNCTIONS);
				switch(randomIndex){
					case SELECTION_ROULETTE:
						return rouletteWheelSelection(this, amountOfParents);
					case SELECTION_TOURNAMENT:
						return tournamentSelection(this, amountOfParents, 2);
					case SELECTION_UNIFORM:
						return stochasticUniversalSelection(this, amountOfParents);
				}
				Logger.log(Logger.LogLevel.ERROR, "Randomly selected selectParents-function faulty!");
				return null;
			}
			
			@Override
			double[] recombine(double[] parent1DNA, double[] parent2DNA) {
				int randomIndex = rng.nextInt(AMOUNT_OF_RECOMBINATION_FUNCTIONS);
				switch(randomIndex){
					case RECOMBINATION_SINGLE_POINT:
						return singlePointCrossover(this, parent1DNA, parent2DNA);
					case RECOMBINATION_SIMPLE_ARITHMETIC:
						return simpleArithmeticRecombination(this, parent1DNA, parent2DNA);
					case RECOMBINATION_SINGLE_ARITHMETIC:
						return singleArithmeticRecombination(this, parent1DNA, parent2DNA);
					case RECOMBINATION_WHOLE_ARITHMETIC:
						return wholeArithmeticRecombination(this, parent1DNA, parent2DNA);
					case RECOMBINATION_BLEND:
						return blendCrossover(this, parent1DNA, parent2DNA);
					case RECOMBINATION_SIMULATED_BINARY:
						return simulatedBinaryCrossover(this, parent1DNA, parent2DNA);
				}
				Logger.log(Logger.LogLevel.ERROR, "Randomly selected recombine-function faulty!");
				return null;
			}
			
			@Override
			double[] mutate(double[] dna) {
				int randomIndex = rng.nextInt(AMOUNT_OF_MUTATION_FUNCTIONS);
				switch(randomIndex){
				case MUTATION_RELATIVE_SINGLE_GENE:
					return singleGeneRelativeMutation(this, dna);
				case MUTATION_UNCORRELATED_1STEP:
					return uncorrelatedMutation1Step(this, dna);
				case MUTATION_UNCORRELATED_NSTEP:
					return uncorrelatedMutationNStep(this, dna);
				case MUTATION_UNIFORM:
					return uniformMutation(this, dna);
				case MUTATION_GAUSSIAN:
					return gaussianMutation(this, dna);
				case MUTATION_GAUSSIAN_SINGLE_GENE:
					return gaussianSingleGeneMutation(this, dna);
				}
				Logger.log(Logger.LogLevel.ERROR, "Randomly selected mutate-function faulty!");
				return null;
			}
		};
			
		return randomPop;
	}
	
	public AbstractPopulation[] getPopulations() { 
		return populations; 
	}
	
}
