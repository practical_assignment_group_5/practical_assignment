import java.util.Arrays;
import java.util.Collections;
import java.util.Properties;
import java.util.Random;
import java.util.stream.DoubleStream;

import org.vu.contest.ContestEvaluation;

/**
 * Object that holds each Organism within the population
 * Contains helper-functions that allow for easier breeding for future generations
 */
public abstract class AbstractPopulation extends SelectionImpl {
	public static int DEFAULT_AMOUNT_PARENTS	= 5;	
	public static int DEFAULT_POP_SIZE 			= 25;	//popSize = amountOfParents ** 2
	
	public static final int DEFAULT_RANDOM_SEED 	= 11;
	
	public static final double MAX_MUTATION_OFFSET 	= 0.5;
	public static double MUTATION_PROBABILITY	= 0.3;
	
	private long seed;
	Random rng;
	int populationSize;
	double lowerBound;	//For this project -5
	double upperBound;	//For this project +5
	
	// Contest-related objects
	private ContestEvaluation evaluation_;
	@SuppressWarnings("unused")
	private int evaluations_limit_;
	private boolean python;
	private int run_limit_;
	private Organism best;
	
	Organism[] pop;
	
	public AbstractPopulation(){
		setParameters();
		rng = new Random(AbstractPopulation.DEFAULT_RANDOM_SEED);
		
		populationSize = AbstractPopulation.DEFAULT_POP_SIZE;
		lowerBound = -5;
		upperBound = 5;
		
		initPopulation();
	}
	
	public AbstractPopulation(long seed){
		setParameters();
		this.seed = seed;
		rng = new Random(this.seed);
		
		populationSize = AbstractPopulation.DEFAULT_POP_SIZE;
		lowerBound = -5;
		upperBound = 5;
		
		initPopulation();
	}
	
	public AbstractPopulation(int popSize){
		setParameters();
		rng = new Random(AbstractPopulation.DEFAULT_RANDOM_SEED);
		
		populationSize = popSize;
		lowerBound = -5;
		upperBound = 5;	
		
		initPopulation();
	}
	
	public AbstractPopulation(int popSize, long seed){
		setParameters();
		this.seed = seed;
		rng = new Random(this.seed);
		
		populationSize = popSize;
		lowerBound = -5;
		upperBound = 5;	
		
		initPopulation();
	}
	
	public AbstractPopulation(int popSize, double lwrBnd, double upprBnd){
		setParameters();
		rng = new Random(AbstractPopulation.DEFAULT_RANDOM_SEED);
		
		populationSize = popSize;
		lowerBound = lwrBnd;
		upperBound = upprBnd;
		
		initPopulation();
	}
	
	public AbstractPopulation(int popSize, double lwrBnd, double upprBnd, long seed){
		setParameters();
		this.seed = seed;
		rng = new Random(this.seed);
		
		populationSize = popSize;
		lowerBound = lwrBnd;
		upperBound = upprBnd;
		
		initPopulation();
	}
	
	public AbstractPopulation(Organism[] population) {
		setParameters();
		rng = new Random(AbstractPopulation.DEFAULT_RANDOM_SEED);
		
		lowerBound = -5;
		upperBound = 5;
		
		this.pop = population;
		populationSize = population.length;
	}
	
	/**
	 * Function that randomly initializes the population, based on the population size. 
	 * Where each organism has genes within the values of the lower- and upper bounds.
	 */
	private void initPopulation(){
		
		pop = new Organism[populationSize];		
		for(int i = 0; i < populationSize; i++){
			pop[i] = new Organism(Organism.DEFAULT_DNA_LENGTH, rng.nextLong());
			pop[i].initDNARandomly(lowerBound, upperBound);
		}
	}
	
	/**
	 * Function that looks for user defined parameters.
	 */
	public void setParameters() {
		if (System.getProperty("populationSize") != null) {
			DEFAULT_POP_SIZE =  Integer.parseInt(System.getProperty("populationSize"));
		}
		if (System.getProperty("amountOfParents") != null) {
			DEFAULT_AMOUNT_PARENTS =  Integer.parseInt(System.getProperty("amountOfParents"));
		}
		if (System.getProperty("mutationProbability") != null) {
			MUTATION_PROBABILITY =  Double.parseDouble(System.getProperty("mutationProbability"));
		}
		if (Boolean.parseBoolean(System.getProperty("python"))) {
			python = Boolean.parseBoolean(System.getProperty("python"));
			run_limit_ = Integer.parseInt(System.getProperty("limits"));
		}
	}
	
	/**
	 * Helper-function that calculates the probability distribution
	 * 
	 * @return double[] probability distribution
	 */
	@Deprecated
	private double[] getProbDist() {
		
		double[] fitnesses = getFitnesses();
		double fitnessSum = DoubleStream.of(fitnesses).sum();
		double[] probOrganisms = new double[populationSize];
		
		// Create probability distribution
		for (int i = 0; i < populationSize; i++) {
			probOrganisms[i] = fitnesses[i] / fitnessSum;
		}	
		return probOrganisms;
	}
	
	
	/**
	 * Helper-function that calculates the cumulative probability distribution
	 * 
	 * @return double[] cumulative probability distribution
	 */
	double[] getCumProbDist() {
		
		double[] fitnesses = getFitnesses();
		double fitnessSum = DoubleStream.of(fitnesses).sum();
		double[] probOrganisms = new double[populationSize];
		
		// Create cumulative probability distribution
		for (int i = 0; i < populationSize; i++) {
			if (i > 0) {
				probOrganisms[i] = probOrganisms[i-1] + (fitnesses[i] / fitnessSum);
			} else { 
				probOrganisms[i] = fitnesses[i] / fitnessSum;
			}
		}	
		return probOrganisms;
	}
	
	/**
	 * Helper-function that calculates, and updates, the fitness of all Organisms in the 
	 * population and arranges them in descending order
	 * 
	 * @return int amount of evaluations executed, likely equal to population size
	 */
	public int calculateFitnesses() {
		int evaluationsPerformed = 0;
		
		// First calculate fitness of each organism
		for(int i = 0; i < populationSize; i++){
			try {
				pop[i].setFitness((double) evaluation_.evaluate(pop[i].getDNA()));
				evaluationsPerformed++;
			} catch(NullPointerException err) {
				//Attempt to fix NullPointer problem
				if(pop[i] == null)
					Logger.log(Logger.LogLevel.ERROR, "Organism at pop[i=" + i + "] is null!");
				else
					Logger.log(Logger.LogLevel.INFORMATION, "Maximum amount of evaluations reached.");
					pop[i].setFitness(-1.0);
				return -1;
			}
		}
		// Sort organisms based on fitness, in descending order
		Arrays.sort(pop, Collections.reverseOrder());
		
		return evaluationsPerformed;
	}
	
	/**
	 * Function that selects the organisms that will be used for breeding the next generation
 	 * 
 	 * @param amountOfParents: amount of parents that will be selected as the possible parents for the next generation
 	 * NOTE: This is not the amount of parents per child!
	 */
	abstract Organism[] selectParents(int amountOfParents);
	
	/**
	 * Helper-function that is called when you want to cross-over the genes of two organisms
	 * 
	 * @param parent1DNA DNA of the first parent
	 * 	Note: this will be used for the first part of the DNA-string
	 * @param parent2DNA DNA of the second parent
	 * 	Note: this will be used for the second part of the DNA-string
	 * @return The new, crossed-over, DNA string
	 */
	abstract double[] recombine(double[] parent1DNA, double[] parent2DNA);
	
	/**
	 * Helper-function that is called when you want to mutate a random gene from the dna
	 * 	Note: Currently a default maximum offset is used to determine how much a gene will be mutated
	 * 
	 * @param dna Array of genes from which one gene will be mutated
	 * @return The new, mutated, DNA string
	 */
	abstract double[] mutate(double[] dna);
	
	/**
	 * Function that creates a new generation of Organisms
	 * 
	 * @param parents Array of Organisms previously selected to have the highest 
	 * 	fitness, where the new generation will be based on
	 * @return Organism-array containing the new generation of Organisms
	 */
	public Organism[] breedNextGeneration(Organism[] parents) {
		
		Organism[] nextGen = new Organism[populationSize];
		for (int i = 0; i < parents.length; i++) {
			nextGen[i] = pop[i];
		}
		Organism child = null;
		int childCount = parents.length-1;
		
		for (int i = 0; i < parents.length; i++) {
			// Currently breeding with each possible pair of parents, except when i==j
			for (int j = 0; j < parents.length; j++) {
				if (i != j) {
					// Copy constructor that copies all the parameters, except random seed
					child = new Organism(parents[i]);
					child.setRNG(rng.nextLong());
					
					// Random single point crossover
					double[] childDNA = recombine(parents[i].getDNA(), parents[j].getDNA());
				
					// Relative single gene mutation
					if(rng.nextDouble() < AbstractPopulation.MUTATION_PROBABILITY) {
						childDNA = mutate(childDNA);
					}
					child.setDNA(childDNA);
					childCount++;
				}
				if(childCount >= 0 && childCount > parents.length-1) {
					nextGen[childCount] = child;
				}
			}
		}
		pop = nextGen;
		return pop;
	}


	/** Getters **/
	public Organism[] getOrganisms(){ return pop; }
	public void setOrganisms(Organism[] pop) { this.pop = pop; }
	public ContestEvaluation getEvaluation() { return evaluation_; }
	public double[] getFitnesses() {	// Note: This is simply a getter, it doesn't calculate the fitness
		double[] fitnesses = new double[populationSize];
		for (int i = 0; i < populationSize; i++) {
			
			fitnesses[i] = pop[i].getFitness();
		}
		return fitnesses;
	}
	
	/**
	 * Calculates the population diversity based on the moment-of-inertia
	 * @return double population diversity
	 */
	public double getDiversity() {
		double[][] organismsDNA = new double[populationSize][pop[0].getDNAStringLength()];
		double[] centroid = new double[pop[0].getDNAStringLength()];
		Arrays.fill(centroid, 0.0);
		
		for(int i = 0; i < populationSize; i++) {
			organismsDNA[i] = pop[i].getDNA();
			for(int d = 0; d < pop[0].getDNAStringLength(); d++) {
				centroid[d] += organismsDNA[i][d];
			}
		}
		
		for(int d = 0; d < pop[0].getDNAStringLength(); d++) {
			centroid[d] = centroid[d] / populationSize;
		}
		
		double diversity = 0.0;
		for(int d = 0; d < pop[0].getDNAStringLength(); d++) {
			for(int i = 0; i < populationSize; i++) {
				double val = organismsDNA[i][d] - centroid[d];
				diversity += val * val;
			}
		}
		
		return diversity/populationSize;
	}
	
	/**
	 * Setter-function that simply allowed the fitness function to be tested within this class
	 * 
	 * @param evaluation the ContestEvaluation passed somewhere in the jar-file
	 */
	public void setEvaluation(ContestEvaluation evaluation)
	{
		// Set evaluation problem used in the run
		evaluation_ = evaluation;
		// Get evaluation properties
		Properties props = evaluation.getProperties();
        // Get evaluation limit
        evaluations_limit_ = Integer.parseInt(props.getProperty("Evaluations"));
    }
	
	/** toString-function
	 * @return String containing information about the population
	 */
	public String toString(){
		return "{Population:[size=" + populationSize 
			+ ", lowerBound=" + lowerBound
			+ ", upperBound=" + upperBound
			+ ", DNA length=" + pop[0].getDNAStringLength() + "]}";
	}
}
