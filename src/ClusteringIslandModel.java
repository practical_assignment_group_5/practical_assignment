import java.util.Arrays;
import java.util.*;

public class ClusteringIslandModel {

	@Deprecated
	public final int DEFAULT_ORGANISMS_PER_POPULATION = 25;
	
	private AbstractPopulation[] populations;
	
	private boolean python;
	private int run_limit_;
	
	private int organismsPerPopulation;
	private int emigrantsPerPopulation;
	
	private int migrationInterval;
	
	private int[] islandTopology;
	
	public ClusteringIslandModel() {
		setParameters();
	}
	
	public ClusteringIslandModel(AbstractPopulation[] populations) {
		setParameters();
		this.populations = populations;
	
		organismsPerPopulation = populations[0].populationSize;
		
		islandTopology = new int[populations.length];
		for(int i = 0; i < populations.length; i++) {
			if((i+1) >= populations.length)
				islandTopology[i] = 0;
			else
				islandTopology[i] = i+1;
		}
		
		//Logger.log(Logger.LogLevel.INFORMATION, "Connections Graph: " + Arrays.toString(islandTopology));
	}
	
	public ClusteringIslandModel(AbstractPopulation[] populations, int amountOfEmigrantsPerPopulation) {
		setParameters();
		this.populations = populations;
	
		islandTopology = new int[populations.length];
		for(int i = 0; i < populations.length; i++) {
			if((i+1) >= populations.length)
				islandTopology[i] = 0;
			else
				islandTopology[i] = i+1;
		}
		
		//Logger.log(Logger.LogLevel.INFORMATION, "Connections Graph: " + Arrays.toString(islandTopology));
	}
	
	/**
	 * Function that looks for user defined parameters.
	 */
	public void setParameters() {
		if (Boolean.parseBoolean(System.getProperty("python"))) {
			python = Boolean.parseBoolean(System.getProperty("python"));
			run_limit_ = Integer.parseInt(System.getProperty("limits"));
		}
	}
	
	/**
	 * Helper-function that calculates the fitness of every organism in every population in @var populations
	 * @return  int total amount of evaluations performed, or -1 either an error occurred or the max amount 
	 * 			of evaluations have been performed
	**/
	public int calculateAllFitnesses(boolean log) {
		double best = 0.0;
		int totalEvalsPerformed = 0;
		for(int i = 0; i < populations.length; i++) {
			int evalsPerformed = populations[i].calculateFitnesses();
			if(evalsPerformed > 0) {
				totalEvalsPerformed += evalsPerformed;
			} else {
				// evalsPerformed <= 0: if pop.calculateFitnesses() encounters an error or max evals is exceeded
				return -1;
			}
			if (log && python) {
				Logger.log(Logger.LogLevel.PYTHON, "\"f_pop_" + (i + 1) + "\": " + populations[i].pop[0].getFitness() + ",");
				Logger.log(Logger.LogLevel.PYTHON, "\"d_pop_" + (i + 1) + "\": " + populations[i].getDiversity() + ",");
			}
			if (populations[i].pop[0].getFitness() > best) {
				best = populations[i].pop[0].getFitness();
			}
		}
		if (log && python) {
			Logger.log(Logger.LogLevel.PYTHON, "\"f_pop_all\": " + best + ",");
			Logger.log(Logger.LogLevel.PYTHON, "\"d_pop_all\": " + getDiversity());
		}
		return totalEvalsPerformed;
	}

	/**
	 * Helper-function that breeds the next generation for all populations of @var populations given 
	 *   the desired parents per population
	 * @param allParents Organism[][] 2d-array containing the desired parents of all populations in @var populations
	 * @return Organism[][] 2d-array containing the organisms of each population's next generation
	**/
	public void breedNextGenerationOfAllPopulations(double parentFraction){
		for(int i = 0; i < populations.length; i++) {
//			int amountOfParents = (int) (parentFraction* ((double) populations[i].populationSize));
//			System.out.println(amountOfParents);
			int amountOfParents = 5;//Math.min(5, populations[i].populationSize);
			Organism[] parents = populations[i].selectParents(amountOfParents);
			populations[i].breedNextGeneration(parents);
		}
	}
	
	/** Getters **/
	public int[] getEmigrationConnections() { return islandTopology; }
	public int getEmigrationInterval() { return migrationInterval; }
	
	/** Setters **/
	public void setEmigrationConnections(int[] emigrationConnections) { this.islandTopology = emigrationConnections; }

	public double getDiversity() {		
//		double dnaLowerBound = Math.abs(populations[0].lowerBound);
//		int dnaLength = populations[0].getOrganisms()[0].getDNAStringLength();
//		
//		
//		double[][] organismsDNA = new double[populations.length * organismsPerPopulation][dnaLength];
//		double[] centroid = new double[dnaLength];
//		Arrays.fill(centroid, 0.0);
//		
//		for(int i = 0; i < populations.length; i++) {
//			Organism[] popOrganisms = populations[i].getOrganisms();
//			for(int j = 0; j < popOrganisms.length; j++) {
//				organismsDNA[i*organismsPerPopulation + j] = popOrganisms[j].getDNA();
//				for(int d = 0; d < dnaLength; d++) {
//					centroid[d] += organismsDNA[i*organismsPerPopulation + j][d] + dnaLowerBound;
//				}
//			}
//		}
//		
//		for(int d = 0; d < dnaLength; d++) {
//			centroid[d] = centroid[d] / organismsDNA.length;
//		}
//		
//		double diversity = 0.0;
//		for(int d = 0; d < dnaLength; d++) {
//			for(int i = 0; i < populations.length; i++) {
//				for(int j = 0; j < organismsPerPopulation; j++) {
//					double val = organismsDNA[i][d] + dnaLowerBound - centroid[d];
//					diversity += val * val;
//				}
//			}
//		}
//		
//		return diversity/organismsDNA.length;
		return 0.0;
	}
	
	public AbstractPopulation[] getPopulations() {
		return populations;
	}
	
}
