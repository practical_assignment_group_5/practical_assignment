import java.util.Arrays;

public class IslandModel {

	@Deprecated
	public final int DEFAULT_ORGANISMS_PER_POPULATION = 25;
	
	public int DEFAULT_EMIGRANTS_PER_POPULATION = 2;
	public int DEFAULT_MIGRATION_INTERVAL = 10;
	
	private AbstractPopulation[] populations;
	
	private boolean python;
	private int run_limit_;
	
	/** 
	 * int denoting the amount of organisms per subpopulation/island
	 *   Note: This is based on the first population in @var populations
	 *   Thus, all population sizes have to be the same.
	**/
	private int organismsPerPopulation;
	private int emigrantsPerPopulation;
	
	/**
	 * int denoting the amount of epochs to wait before doing another migration 
	 *   Note: This variable is only used in the player-loop, not in this class.
	**/
	private int migrationInterval;
	
	/** 
	 * int array containing the i-th island's emigrant's destination
	 *   Note: currently implemented to have only one connection per island
	**/
	private int[] islandTopology;
	
	public IslandModel() {
		// Default constructor
		setParameters();
	}
	
	public IslandModel(AbstractPopulation[] populations) {
		setParameters();
		this.populations = populations;
	
		organismsPerPopulation = populations[0].populationSize;
		emigrantsPerPopulation = DEFAULT_EMIGRANTS_PER_POPULATION;
		migrationInterval = DEFAULT_MIGRATION_INTERVAL;
		
		islandTopology = new int[populations.length];
		for(int i = 0; i < populations.length; i++) {
			if((i+1) >= populations.length)
				islandTopology[i] = 0;
			else
				islandTopology[i] = i+1;
		}
		
		Logger.log(Logger.LogLevel.INFORMATION, "Connections Graph: " + Arrays.toString(islandTopology));
	}
	
	public IslandModel(AbstractPopulation[] populations, int amountOfEmigrantsPerPopulation) {
		setParameters();
		this.populations = populations;
	
		organismsPerPopulation = populations[0].populationSize;
		emigrantsPerPopulation = amountOfEmigrantsPerPopulation;
		migrationInterval = DEFAULT_MIGRATION_INTERVAL;
		
		islandTopology = new int[populations.length];
		for(int i = 0; i < populations.length; i++) {
			if((i+1) >= populations.length)
				islandTopology[i] = 0;
			else
				islandTopology[i] = i+1;
		}
		
		Logger.log(Logger.LogLevel.INFORMATION, "Connections Graph: " + Arrays.toString(islandTopology));
	}
	
	/**
	 * Function that looks for user defined parameters.
	 */
	public void setParameters() {
		if (System.getProperty("emigrantsPerPopulation") != null) {
			DEFAULT_EMIGRANTS_PER_POPULATION =  Integer.parseInt(System.getProperty("emigrantsPerPopulation"));
		}
		if (System.getProperty("migrationInterval") != null) {
			DEFAULT_MIGRATION_INTERVAL =  Integer.parseInt(System.getProperty("migrationInterval"));
		}
		if (Boolean.parseBoolean(System.getProperty("python"))) {
			python = Boolean.parseBoolean(System.getProperty("python"));
			run_limit_ = Integer.parseInt(System.getProperty("limits"));
		}
	}
	
	/**
	 * Helper-function that calculates the fitness of every organism in every population in @var populations
	 * @return  int total amount of evaluations performed, or -1 either an error occurred or the max amount 
	 * 			of evaluations have been performed
	**/
	public int calculateAllFitnesses(boolean log) {
		double best = 0.0;
		int totalEvalsPerformed = 0;
		for(int i = 0; i < populations.length; i++) {
			int evalsPerformed = populations[i].calculateFitnesses();
			if(evalsPerformed > 0) {
				totalEvalsPerformed += evalsPerformed;
			} else {
				// evalsPerformed <= 0: if pop.calculateFitnesses() encounters an error or max evals is exceeded
				return -1;
			}
			if (log && python) {
				Logger.log(Logger.LogLevel.PYTHON, "\"f_pop_" + (i + 1) + "\": " + populations[i].pop[0].getFitness() + ",");
				Logger.log(Logger.LogLevel.PYTHON, "\"d_pop_" + (i + 1) + "\": " + populations[i].getDiversity() + ",");
			}
			if (populations[i].pop[0].getFitness() > best) {
				best = populations[i].pop[0].getFitness();
			}
		}
		if (log && python) {
			Logger.log(Logger.LogLevel.PYTHON, "\"f_pop_all\": " + best + ",");
			Logger.log(Logger.LogLevel.PYTHON, "\"d_pop_all\": " + getDiversity());
		}
		return totalEvalsPerformed;
	}
	
	/**
	 * Helper-function that selects all parents of all populations of @var populations, using each
	 *   population's own selection-method
	 * @param int amountOfParentsPerPopulation desired amount of parents to be selected per population
	 * @return Organism[][] 2d-array containing the parents of the i-th subpopulation in allParents[i][..]
	**/
	public Organism[][] selectParentsOfAllPopulations(int amountOfParentsPerPopulation) {
		Organism[][] allParents = new Organism[populations.length][amountOfParentsPerPopulation];
		for(int i = 0; i < populations.length; i++) {
			allParents[i] = populations[i].selectParents(amountOfParentsPerPopulation);
		}
		return allParents;
	}
	
	/**
	 * Helper-function that breeds the next generation for all populations of @var populations given 
	 *   the desired parents per population
	 * @param allParents Organism[][] 2d-array containing the desired parents of all populations in @var populations
	 * @return Organism[][] 2d-array containing the organisms of each population's next generation
	**/
	public Organism[][] breedNextGenerationOfAllPopulations(Organism[][] allParents){
		Organism[][] nextGenerations = new Organism[populations.length][organismsPerPopulation];
		
		for(int i = 0; i < populations.length; i++) {
			nextGenerations[i] = populations[i].breedNextGeneration(allParents[i]);
		}
		
		return nextGenerations;
	}
	
	/**
	 * Helper-function that selects @var emigrantsPerPopulation emigrants of the given population, using
	 *   the {random-to-random} migration policy
	 * @param pop AbstractPopulation subpopulation of which you want to select the emigrants
	 * @return Organism[] array containing the selected emigrants of the @var pop population
	**/
	private Organism[] selectEmigrantsFromPopulation(AbstractPopulation pop) {		
		// Rand-Rand migration
		Organism[] emigrants = new Organism[emigrantsPerPopulation];
		Organism[] popOrganisms = pop.getOrganisms();
		for(int j = 0; j < emigrantsPerPopulation; j++) {
			int randomIndex = pop.rng.nextInt(popOrganisms.length);
			emigrants[j] = popOrganisms[randomIndex];
		}
		
		return emigrants;
	}
	
	/**
	 * Helper-function that retrieves the indices of each emigrant in the given @var emigrants array in 
	 *   @var pop population
	 * @param pop AbstractPopulation subpopulation from which to get the indices of the emigrants
	 * @param emigrants Organism[] array containing the emigrants of which you want to find the indices
	 * @return int[] array containing the indices of the given emigrants in the given population
	**/
	private int[] getIndicesOfPopulationFromEmigrants(AbstractPopulation pop, Organism[] emigrants) {
		Organism[] organisms = pop.getOrganisms();
		int[] emigrantIndices = new int[emigrantsPerPopulation];
		for(int j = 0; j < emigrants.length; j++) {
			int emigrantIndex = Arrays.asList(organisms).indexOf(emigrants[j]);
			if(emigrantIndex != -1) {
				emigrantIndices[j] = emigrantIndex;
			} else {
				// Object not found in array
				Logger.log(Logger.LogLevel.ERROR, "Organism not found in array!");
			}
		}
		return emigrantIndices;
	}
	
	/**
	 * Function that executes the entire migration-process
	 * First selects the emigrants of each populations, followed by getting their indices,
	 *   after which the emigrants get migrated and finally setting each population's organisms
	 *   to the new states
	**/
	public void migrate() {
		
		// Get all organisms and all emigrants of each population
		Organism[][] allOrganisms = new Organism[populations.length][organismsPerPopulation];
		Organism[][] emigrants = new Organism[populations.length][emigrantsPerPopulation];
		for(int i = 0; i < populations.length; i++) {
			allOrganisms[i] = populations[i].getOrganisms();
			emigrants[i] = selectEmigrantsFromPopulation(populations[i]);
		}
		
		// Get all indices of the emigrants of all populations
		int[][] emigrantIndicesPerPopulation = new int[populations.length][emigrantsPerPopulation];
		for(int i = 0; i < populations.length; i++) {
			emigrantIndicesPerPopulation[i] = getIndicesOfPopulationFromEmigrants(populations[i], emigrants[i]);
		}

		// Replace the organisms at the destination population's emigrants' indices
		// with the emigrant from the original population.
		for(int i = 0; i < populations.length; i++) {
			for(int j = 0; j < emigrantsPerPopulation; j++) {
				allOrganisms[islandTopology[i]][emigrantIndicesPerPopulation[i][j]] 
						= emigrants[i][j];
			}
		}
		
		// Actually set the populations' new Organisms
		for(int i = 0; i < populations.length; i++) {
			populations[i].setOrganisms(allOrganisms[i]);
		}
	}
	
	/** Getters **/
	public int[] getEmigrationConnections() { return islandTopology; }
	public int getEmigrationInterval() { return migrationInterval; }
	
	/** Setters **/
	public void setEmigrationConnections(int[] emigrationConnections) { this.islandTopology = emigrationConnections; }

	public double getDiversity() {		
		double dnaLowerBound = Math.abs(populations[0].lowerBound);
		int dnaLength = populations[0].getOrganisms()[0].getDNAStringLength();
		double[][] organismsDNA = new double[populations.length * organismsPerPopulation][dnaLength];
		double[] centroid = new double[dnaLength];
		Arrays.fill(centroid, 0.0);
		
		for(int i = 0; i < populations.length; i++) {
			Organism[] popOrganisms = populations[i].getOrganisms();
			for(int j = 0; j < popOrganisms.length; j++) {
				organismsDNA[i*organismsPerPopulation + j] = popOrganisms[j].getDNA();
				for(int d = 0; d < dnaLength; d++) {
					centroid[d] += organismsDNA[i*organismsPerPopulation + j][d] + dnaLowerBound;
				}
			}
		}
		
		for(int d = 0; d < dnaLength; d++) {
			centroid[d] = centroid[d] / organismsDNA.length;
		}
		
		double diversity = 0.0;
		for(int d = 0; d < dnaLength; d++) {
			for(int i = 0; i < populations.length; i++) {
				for(int j = 0; j < organismsPerPopulation; j++) {
					double val = organismsDNA[i][d] + dnaLowerBound - centroid[d];
					diversity += val * val;
				}
			}
		}
		
		return diversity/organismsDNA.length;
	}
	
}
