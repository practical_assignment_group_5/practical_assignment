import java.util.Random;

public class ExamplePopulation extends AbstractPopulation {

	/**
	 * Constructor overloading
	**/
	public ExamplePopulation(){
		super();
	}
	
	public ExamplePopulation(long seed){
		super(seed);
	}
	
	public ExamplePopulation(int popSize){
		super(popSize);
	}
	
	public ExamplePopulation(int popSize, long seed){
		super(popSize, seed);
	}
	
	public ExamplePopulation(int popSize, double lwrBnd, double upprBnd){
		super(popSize, lwrBnd, upprBnd);
	}
	
	public ExamplePopulation(int popSize, double lwrBnd, double upprBnd, long seed){
		super(popSize, lwrBnd, upprBnd, seed);
	}
	
	public ExamplePopulation(Organism[] population) {
		super(population);
	}
	
	@Override
	public Organism[] selectParents(int amountOfParents){
		String selectionMethod = "";
		if (System.getProperty("selection") != null) {
			selectionMethod =  System.getProperty("selection");
		}
//		Logger.log(Logger.LogLevel.PYTHON, selectionMethod);
		if (selectionMethod.equals("roulette")) {
//			Logger.log(Logger.LogLevel.PYTHON,  "heuj");
			return rouletteWheelSelection(this, amountOfParents);
		} else if ( selectionMethod.equals("stochastic")) {
			return stochasticUniversalSelection(this, amountOfParents);
		} else if ( selectionMethod.equals("tournament")) {
			return tournamentSelection(this, amountOfParents, 2);
		}
		return rouletteWheelSelection(this, amountOfParents);
	}
	
	@Override 
	double[] recombine(double[] parent1DNA, double[] parent2DNA) {
		String recombinationMethod = "";
		if (System.getProperty("crossover") != null) {
			recombinationMethod =  System.getProperty("crossover");
		}
//		Logger.log(Logger.LogLevel.PYTHON, recombinationMethod);
		if (recombinationMethod.equals("singlePoint")) {
			return singlePointCrossover(this, parent1DNA, parent2DNA);
		} else if (recombinationMethod.equals("singleArithmetic")) {
			return singleArithmeticRecombination(this, parent1DNA, parent2DNA);
		} else if (recombinationMethod.equals("simpleArithmetic")) {
			return simpleArithmeticRecombination(this, parent1DNA, parent2DNA);
		} else if (recombinationMethod.equals("wholeArithmetic")) {
			return wholeArithmeticRecombination(this, parent1DNA, parent2DNA);
		} else if (recombinationMethod.equals("blend")) {
			return blendCrossover(this, parent1DNA, parent2DNA);
		} else if (recombinationMethod.equals("simulatedBinary")) {
			return simulatedBinaryCrossover(this, parent1DNA, parent2DNA);
		}
		return singlePointCrossover(this, parent1DNA, parent2DNA);
		
	}
	
	@Override
	double[] mutate(double[] dna) {
		String mutationMethod = "";
		if (System.getProperty("mutation") != null) {
			mutationMethod =  System.getProperty("mutation");
		}
//		Logger.log(Logger.LogLevel.PYTHON, mutationMethod);
		if (mutationMethod.equals("relativeSingleGene")) {
			return singleGeneRelativeMutation(this, dna);
		} else if (mutationMethod.equals("uncorrelated1")) {
			return uncorrelatedMutation1Step(this, dna);
		} else if (mutationMethod.equals("uncorrelatedN")) {
			return uncorrelatedMutationNStep(this, dna);
		} else if (mutationMethod.equals("uniform")) {
			return uniformMutation(this, dna);
		} else if (mutationMethod.equals("gaussian")) {
			return gaussianMutation(this, dna);
		} else if (mutationMethod.equals("gaussianSingleGene")) {
			return gaussianSingleGeneMutation(this, dna);
		}
		return gaussianSingleGeneMutation(this, dna);
	}
	
	@Override
	/**
	 * Function that creates a new generation of Organisms.
	 * 
	 * Uses tournament selection to get best individuals of all parents and children to survive
	 * to the next generation.
	 * 
	 * @param parents Array of Organisms previously selected to have the highest 
	 * 	fitness, where the new generation will be based on
	 * @return Organism-array containing the new generation of Organisms
	 */
	public Organism[] breedNextGeneration(Organism[] parents) {
	
		Organism child = null;
		Organism[] total = new Organism[pop.length + ((parents.length) * (parents.length - 1))];
		for (int i = 0; i < this.pop.length; i++) {
			total[i] = pop[i];
		}
		int childCount = pop.length;
		
		for (int i = 0; i < parents.length; i++) {
			// Currently breeding with each possible pair of parents, except when i==j
			for (int j = 0; j < parents.length; j++) {
				if (i != j) {
					// Copy constructor that copies all the parameters, except random seed
					child = new Organism(parents[i]);
					child.setRNG(rng.nextLong());
					
					// Random single point crossover
					double[] childDNA = recombine(parents[i].getDNA(), parents[j].getDNA());
				
					// Relative single gene mutation
					if(rng.nextDouble() < AbstractPopulation.MUTATION_PROBABILITY) {
						childDNA = mutate(childDNA);
					}
					child.setDNA(childDNA);
					total[childCount] = child;
					childCount++;
				}
			}
		}
		this.pop = total;
		
		Organism[] nextGen = tournamentSelection(this, populationSize, 2);
		pop = nextGen;
		return pop;
	}
		
}
