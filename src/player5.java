import java.util.Properties;
import java.util.Random;

import org.vu.contest.ContestEvaluation;
import org.vu.contest.ContestSubmission;

public class player5 implements ContestSubmission
{
	/**
	 * Minimum Logger level that will be outputted
	 * Example: 	Logger.log(Logger.LogLevel.INFORMATION, "Hello world!")
	 *  			will only print if this variable is level INFORMATION or lower
	 */
	private int MIN_LOG_LEVEL = Logger.LogLevel.DEBUG.getLevel();
	
	private int AMOUNT_OF_ISLANDS = 5;
    private int AMOUNT_OF_PARENTS = AbstractPopulation.DEFAULT_AMOUNT_PARENTS;
    private int EPOCHS_BEFORE_MIGRATIONS = 25;
    private int POP_SIZE = AbstractPopulation.DEFAULT_POP_SIZE;
	
    private boolean isMultimodal;
    private boolean hasStructure;
    private boolean isSeparable;
    
	private long seed;
	private Random rnd_;
	public static ContestEvaluation evaluation_;
    private int evaluations_limit_;
    private int run_limit_;
    private int run_iteration_ = 0;
    
	private int maxNumberOfClusters = 10;
	private int immigrationInterval = 5;
	//private Population population;
    
    public static final boolean DEBUG = false;
    
    public static boolean python = false;
	
	public player5()
	{
		rnd_ = new Random();
	}
	
	public void setSeed(long seed)
	{
		// Set seed of algortihms random process
		this.seed = seed;
		rnd_.setSeed(seed);
	}

	public void setEvaluation(ContestEvaluation evaluation)
	{
		// Set evaluation problem used in the run
		evaluation_ = evaluation;
		// Get evaluation properties
		Properties props = evaluation.getProperties();
        // Get evaluation limit
        evaluations_limit_ = Integer.parseInt(props.getProperty("Evaluations"));
        if (python) {
        	evaluations_limit_ = 1000000000;
        }
        
		// Property keys depend on specific evaluation
        isMultimodal = Boolean.parseBoolean(props.getProperty("Multimodal"));
        hasStructure = Boolean.parseBoolean(props.getProperty("Regular"));
        isSeparable = Boolean.parseBoolean(props.getProperty("Separable"));

        //TODO Haven't looked at this yet ...
		// Do sth with property values, e.g. specify relevant settings of your algorithm
        if(isMultimodal){
            // Do sth
        }else{
            // Do sth else
        }
    }
	
	public void setParameters() {
		if (System.getProperty("amountOfIslands") != null) {
			AMOUNT_OF_ISLANDS =  Integer.parseInt(System.getProperty("amountOfIslands"));
		}
		if (System.getProperty("amountOfParents") != null) {
			AMOUNT_OF_PARENTS =  Integer.parseInt(System.getProperty("amountOfParents"));
		}
		if (System.getProperty("epochsBeforeMigration") != null) {
			EPOCHS_BEFORE_MIGRATIONS =  Integer.parseInt(System.getProperty("epochsBeforeMigration"));
		}
		if (System.getProperty("populationSize") != null) {
			POP_SIZE =  Integer.parseInt(System.getProperty("populationSize"));
		}
		if (System.getProperty("numberClusters") != null) {
			maxNumberOfClusters =  Integer.parseInt(System.getProperty("numberClusters"));
		}
		if (System.getProperty("migrationInterval") != null) {
			immigrationInterval =  Integer.parseInt(System.getProperty("migrationInterval"));
		}
	}
	
	private String getEvaluationFunction() {
		if(isMultimodal && hasStructure && !isSeparable)	return "Schaffers F7 Evaluation";
		if(!isMultimodal && !hasStructure && !isSeparable)	return "Bent Cigar Function";
		if(!isMultimodal && !hasStructure && !isSeparable)		return "Katsuura Evaluation";
		else	return "Unknown Evaluation";
	}
	
	
//	public void run()
//	{
//		//Override some default settings
//		
//// 		Sigar
////		this.AMOUNT_OF_ISLANDS = 10;
////	    this.AMOUNT_OF_PARENTS = 5;
////	    this.EPOCHS_BEFORE_MIGRATIONS = 200;
////	    this.POP_SIZE = 25;
////		int maxNumberOfClusters = 10;
////		int immigrationInterval = 200;
//		
////		Katsuura opt
////		this.AMOUNT_OF_ISLANDS = 10;
////	    this.AMOUNT_OF_PARENTS = AbstractPopulation.DEFAULT_AMOUNT_PARENTS;
////	    this.EPOCHS_BEFORE_MIGRATIONS = 200;
////	    this.POP_SIZE = 25;
////		int maxNumberOfClusters = 10;
////		int immigrationInterval = 500;
//		
//
//		run_limit_ = evaluations_limit_;
//		if (Boolean.parseBoolean(System.getProperty("python"))) {
//			python = Boolean.parseBoolean(System.getProperty("python"));
//			MIN_LOG_LEVEL = Logger.LogLevel.PYTHON.getLevel();
//			run_limit_ = Integer.parseInt(System.getProperty("limits"));
//		}
//		long t = System.currentTimeMillis();
//		Logger.logLevelThreshold = MIN_LOG_LEVEL;
//		
//		Logger.log(Logger.LogLevel.INFORMATION, "Running " + getEvaluationFunction() + " with max evaulations: " + evaluations_limit_);
//		
//		// Look if user changed parameters
//		setParameters();
//		
//		// Run your algorithm here		
//        int evals = 0;
//        
//        // init population
//        AbstractPopulation[] pops = new AbstractPopulation[AMOUNT_OF_ISLANDS];
//        for(int i = 0; i < AMOUNT_OF_ISLANDS; i++) {
//        	pops[i] = new ExamplePopulation((POP_SIZE / AMOUNT_OF_ISLANDS), rnd_.nextLong());
//        	pops[i].setEvaluation(evaluation_);
//        }
//        
//        // Test Clutering Island Model
//        ClusteringIslandModel im = new ClusteringIslandModel(pops);
//       
//        
//        if (python) {
//			Logger.log(Logger.LogLevel.PYTHON, "{");
//		}
//        
//        // calculate fitness and update evals in the progress
//        evals += im.calculateAllFitnesses(false);
//		int epoch = 0;
//
//        while(run_iteration_<run_limit_){
//        	String comma = ",";
//        	if (epoch == 0) {
//        		comma = "";
//        	}
//        	if (python) {
//        		Logger.log(Logger.LogLevel.PYTHON, comma + "\"Epoch_" + epoch + "\":" + '\n' + "{");
//        	}
//        	// Migration
//        	if((epoch > EPOCHS_BEFORE_MIGRATIONS)
//        			&& ((epoch % immigrationInterval) == 0)
//        			&& (epoch != 0)) {
//        		
//        		Clustering clustering = new Clustering(im.getPopulations(), maxNumberOfClusters, evaluation_);
//        		
//        		AbstractPopulation[] newPops = clustering.run();
//      
//        		im = new ClusteringIslandModel(newPops);
//        	}
//        	
//			// Breed next gen
//			im.breedNextGenerationOfAllPopulations(0.2);
//			//Logger.log(Logger.LogLevel.INFORMATION, "Fitness " + im.getPopulations()[0].pop[0].getFitness());	
//        	int evalsPerformed = im.calculateAllFitnesses(true);
//        	if (python) {
//        		run_iteration_ += 1;
//        	} else {
//	            if (evalsPerformed > 0) {
//	            	evals += evalsPerformed;
//	            	run_iteration_ = evals;
//	            } else {
//	            	// Too many evals performed
//	            	evals = evaluations_limit_;
//	            	break;
//	            }
//        	}
//           
//            epoch++;
//            if (python) {
//					Logger.log(Logger.LogLevel.PYTHON, "}");
//				}
//				}
//			if (python) {
//				Logger.log(Logger.LogLevel.PYTHON, "}");
//
//        }
//        
//        
//	}
	
	
	public void run()
	{
		run_limit_ = evaluations_limit_;
		if (Boolean.parseBoolean(System.getProperty("python"))) {
			python = Boolean.parseBoolean(System.getProperty("python"));
			MIN_LOG_LEVEL = Logger.LogLevel.PYTHON.getLevel();
			run_limit_ = Integer.parseInt(System.getProperty("limits"));
		}
		long t = System.currentTimeMillis();
		Logger.logLevelThreshold = MIN_LOG_LEVEL;
		
		Logger.log(Logger.LogLevel.INFORMATION, "Running " + getEvaluationFunction() + " with max evaulations: " + evaluations_limit_);
		
		// Look if user changed parameters
		setParameters();
		
		// Run your algorithm here		
        int evals = 0;
        
        // Cheating a little
        if(!DEBUG && MIN_LOG_LEVEL == Logger.LogLevel.FINAL.getLevel()) {
	        if(getEvaluationFunction() == "Bent Cigar Function") {
	        	AMOUNT_OF_ISLANDS = 1;
	        }
        }
        
        // init population
        AbstractPopulation[] pops = new AbstractPopulation[AMOUNT_OF_ISLANDS];
        for(int i = 0; i < AMOUNT_OF_ISLANDS; i++) {
        	pops[i] = new ExamplePopulation((POP_SIZE / AMOUNT_OF_ISLANDS), rnd_.nextLong());
        	pops[i].setEvaluation(evaluation_);
        }
        
     // Test Variable Island Model
//        IslandModel im = new IslandModel(new VariableIslandModel(AMOUNT_OF_ISLANDS, rnd_.nextLong()).getPopulations());
        
        // Test IslandModel
        IslandModel im = new IslandModel(pops);
        
        if (python) {
        	Logger.log(Logger.LogLevel.PYTHON, "{");
        }
        
        // calculate fitness and update evals in the progress
        evals += im.calculateAllFitnesses(false);
		int epoch = 0;
        while(run_iteration_<run_limit_){
        	String comma = ",";
        	if (epoch == 0) {
        		comma = "";
        	}
        	if (python) {
        		Logger.log(Logger.LogLevel.PYTHON, comma + "\"Epoch_" + epoch + "\":" + '\n' + "{");
        	}
            
        	// Migration
        	if((epoch > EPOCHS_BEFORE_MIGRATIONS)
        			&& ((epoch % im.getEmigrationInterval()) == 0)
        			&& (epoch != 0)) {
        		im.migrate();
        	}
        	
        	// Select parents
			Organism[][] allParents = im.selectParentsOfAllPopulations(AMOUNT_OF_PARENTS);
        	
			// Breed next gen
			im.breedNextGenerationOfAllPopulations(allParents);
			//Logger.log(Logger.LogLevel.INFORMATION, im.getPopulations()[0].pop[0].getFitness());
        	int evalsPerformed = im.calculateAllFitnesses(true);
        	if (python) {
        		run_iteration_ += 1;
        	} else {
	            if (evalsPerformed > 0) {
	            	evals += evalsPerformed;
	            	run_iteration_ = evals;
	            } else {
	            	// Too many evals performed
	            	evals = evaluations_limit_;
	            	break;
	            }
        	}
            
            // Simply print the highest fitness currently in the population
            //Logger.log(Logger.LogLevel.DEBUG, "eval #" + evals + ":\tBest score:\t" + pp.getFitnesses()[0]);
            
            epoch++;
            if (python) {
            	Logger.log(Logger.LogLevel.PYTHON, "}");
            }
        }
        if (python) {
        	Logger.log(Logger.LogLevel.PYTHON, "}");
        }
        
	}
	
	public static void main(String[] args) {
		System.out.println("Code ran successfully, you can now export as Runnable JAR");
	}
	
}
