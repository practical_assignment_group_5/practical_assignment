
public class SelectionImpl extends MutationImpl{
	
	protected Organism[] rouletteWheelSelection(AbstractPopulation pop, int amountOfParents) {
		Organism[] parents = new Organism[amountOfParents];
		//Algorithm for selecting parents
		//e.g. Roulette wheel:
		//	Probability of being selected is based on your fitness compared to the total fitness
		//	Note: 	When initializing the population, the fitness of most will be xE-10 or even lower.
		//			This means that we will VERY likely pick the same organism as every parent...
		//			Alternatives to consider are:
		//				* Stochastic universal sampling (atleast to begin with)
		//				* Tournament selection
		
		// Get cumulative probability distribution
		double[] probOrganisms = pop.getCumProbDist();
		
		// 	Actually select parents through inverse cumulative probability
		// 	Note: This is selection with replacement!
		// 		Because well.., we hope to get the best parents, even
		//		if that means we'll just need to get lucky on mutations
		for (int i = 0; i < amountOfParents; i++) {
			double r = pop.rng.nextDouble();
			parents[i] = null;
			int j;
			for (j = 0; j < pop.populationSize; j++) {
				if (r <= probOrganisms[j]) {
					break;
				}
			}
			parents[i] = pop.pop[j];
		}
		
		return parents;
	}
	
	protected Organism[] tournamentSelection(AbstractPopulation pop, int amountOfParents, int amountCompetitors) {
		Organism[] parents = new Organism[amountOfParents];

		for (int i = 0; i < amountOfParents; i++) {
			Organism best = null;

			// Checks for some random organisms if they have a greater fitness then the current best.
			for (int j = 0; j < amountCompetitors; j++) {
				int nextCandidate = pop.rng.nextInt(pop.pop.length);
				Organism candidate = pop.pop[nextCandidate];
				if (best == null || candidate.getFitness() > best.getFitness()) {
					best = candidate;
				}
			}
			parents[i] = best;
		}
		return parents;
	}
	
	protected Organism[] stochasticUniversalSelection(AbstractPopulation pop, int amountOfParents) {
		// Same as roulette wheel, but the wheel has multiple arms which are spinned all at once. This way it's more
		// likely multiple parents are selected.

		Organism[] parents = new Organism[amountOfParents];
		
		// Get cumulative probability distribution		
		double[] probOrganisms = pop.getCumProbDist();

		double r = pop.rng.nextDouble() / amountOfParents;
		int i = 0;
		int j = 0;
		while (i < amountOfParents) {
			while (r <= probOrganisms[j]) {
				parents[i] = pop.pop[j];
				r += 1.0 / amountOfParents;
				i += 1;
			}
			j += 1;
		}
		return parents;
	}
	
	protected Organism[] anyOtherSelection(AbstractPopulation pop, int amountOfParents) {
		Organism[] parents = new Organism[amountOfParents];
		// Some selection
		return parents;
	}
	
}
