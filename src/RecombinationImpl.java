
public class RecombinationImpl {

	protected double[] singlePointCrossover(AbstractPopulation pop, double[] parent1DNA, double[] parent2DNA) {
		int crossoverPoint = pop.rng.nextInt(parent1DNA.length); // [0 ... dna.length-1]	
		//	Note: 	The crossoverPoint can be 0, meaning that only the first gene will be from the first
		//			parent. But it can also be dna.length-1, meaning that all genes from the second parent
		//			will be used.
		
		double[] childDNA = new double[parent1DNA.length];
		for (int i = 0; i < childDNA.length; i++) {
			if(i <= crossoverPoint)	{
				childDNA[i] = parent1DNA[i];
			} else {
				childDNA[i] = parent2DNA[i];
			}
		}
		return childDNA;
	}
	
	/* FLOATING
	 * Function for the simple arithmetic recombination. Genes after a crossover point will be the average
	 * of the parents.
	 */
	protected double[] simpleArithmeticRecombination(AbstractPopulation pop, double[] parent1DNA, double[] parent2DNA) {
		int crossoverPoint = pop.rng.nextInt(parent1DNA.length);
		//	Note: 	The crossoverPoint can be 0, meaning that only the first gene will be from the first
		//			parent. But it can also be dna.length-1, meaning that all genes from the first parent
		//			will be used.
		double alpha = pop.rng.nextDouble();
		//	Note:	alpha is a scaling factor how much the child will resemble first or second parent. When
		//			alpha is 0.5, the average is taken from the parents.
		
		double[] childDNA = new double[parent1DNA.length];
		for (int i = 0; i < childDNA.length; i++) {
			if(i <= crossoverPoint)	{
				childDNA[i] = parent1DNA[i];
			} else {
				childDNA[i] = alpha * parent1DNA[i] + (1 - alpha) * parent2DNA[i];
			}
		}
		return childDNA;
	}
	
	/* FLOATING
	 * Function for the single arithmetic recombination. A random chosen gene will be the average of the
	 * parents. Rest of the DNA of parent1 will be used.
	 */
	protected double[] singleArithmeticRecombination(AbstractPopulation pop, double[] parent1DNA, double[] parent2DNA) {
		int crossoverPoint = pop.rng.nextInt(parent1DNA.length);
		double alpha = pop.rng.nextDouble();
		//	Note:	Alpha is a scaling factor how much the child will resemble first or second parent. When
		//			alpha is 0.5, the average is taken from the parents.
		
		double[] childDNA = new double[parent1DNA.length];
		for(int i = 0; i < parent1DNA.length; i++) {
			childDNA[i] = parent1DNA[i];
		}
		childDNA[crossoverPoint] = alpha * parent1DNA[crossoverPoint] + (1 - alpha) * parent2DNA[crossoverPoint];
		return childDNA;
	}
	
	/* FLOATING
	 * Function for the whole arithmetic recombination. All genes of the child will be the average of
	 * the parents.
	 */
	protected double[] wholeArithmeticRecombination(AbstractPopulation pop, double[] parent1DNA, double[] parent2DNA) {
		double alpha = pop.rng.nextDouble();
		//	Note:	Alpha is a scaling factor how much the child will resemble first or second parent. When
		//			alpha is 0.5, the average is taken from the parents.
		
		double[] childDNA = new double[parent1DNA.length];
		for (int i = 0; i < childDNA.length; i++) {
			childDNA[i] = alpha * parent1DNA[i] + (1 - alpha) * parent2DNA[i];
		}
		return childDNA;
	}
	
	/* FLOATING
	 * Function for blend recombination. Recombination is executed for every gene.
	 */
	protected double[] blendCrossover(AbstractPopulation pop, double[] parent1DNA, double[] parent2DNA) {
		double alpha = pop.rng.nextDouble();
		double u = pop.rng.nextDouble();
		double gamma = (1 - 2 * alpha) * u - alpha;
		//	Note:	Best results for balancing exploration and exploitation when alpha = 0.5.
		
		double[] childDNA = new double[parent1DNA.length];
		for (int i = 0; i < parent1DNA.length; i++) {
			childDNA[i] = (1 - gamma) * parent1DNA[i] + gamma * parent2DNA[i];
		}
		return childDNA;
	}
	
	/* FLOATING
	 * Function for simulated binary recombination. Recombination is executed for every gene.
	 */
	protected double[] simulatedBinaryCrossover(AbstractPopulation pop, double[] parent1DNA, double[] parent2DNA) {
		double eta = 10 * pop.rng.nextDouble();
		double u = pop.rng.nextDouble();
		double beta = 0;
		
		// Note: beware, Math.pow(..) is a computationally expensive function!
		if(u <= 0.5) {
			beta = Math.pow((2 * u), 1 / (1 + eta));
		} else {
			beta = Math.pow((1 / (2 * (1 - u))), (1 / (1 + eta)));
		}
		
		double[] childDNA = new double[parent1DNA.length];
		for (int i = 0; i < parent1DNA.length; i++) {
			childDNA[i] = 0.5 * ((1 + beta) * parent1DNA[i] + (1 - beta) * parent2DNA[i]);
		}
		return childDNA;
	}
	
	/*
	 * 
	 */
	protected double[] someRecombination(AbstractPopulation pop, double[] parent1DNA, double[] parent2DNA) {
		double[] childDNA = null;
		// Some recombination
		return childDNA;
	}
	
}
