public class Coordinate {

    private double[] values;
    private int organismNumber;

    public Coordinate(double[] values, int organismNumber) {
        this.values = values;
        this.organismNumber = organismNumber;
    }

    public double[] getValues() {
        return values;
    }

    public int getValuesLength(){
        return this.values.length;
    }

    public Coordinate increaseCoordinate(Coordinate org){
        double[] newValues = new double[org.getValuesLength()];
        for(int i =0; i < org.getValuesLength(); i++){
            newValues[i] = values[i] + org.getValues()[i];
        }
        return new Coordinate(newValues, organismNumber);
    }

    public Coordinate decreaseCoordinate(Coordinate org){
        double[] newValues = new double[org.getValuesLength()];
        for(int i =0; i < org.getValuesLength(); i++){
            newValues[i] = values[i] - org.getValues()[i];
        }
        return new Coordinate(newValues, organismNumber);
    }

    public Coordinate div(double d) {
        double[] newValues = new double[values.length];
        for(int i =0; i < values.length; i++){
            newValues[i] = values[i] / d;
        }
        return new Coordinate(newValues, organismNumber);
    }

    public double getModulus() {
        double diff_square_sum = 0.0;
        for (int i = 0; i < getValuesLength(); i++) {
            diff_square_sum += (values[i] * values[i]);
        }
        return Math.sqrt(diff_square_sum);
    }

    public int getOrganismNumber() {
        return organismNumber;
    }
}
