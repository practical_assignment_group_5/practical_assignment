import java.util.Arrays;
import java.util.Random;

/**
 * Object that holds the DNA of an organism
 * Includes helper-functions for initializing, calculating fitness, etc.
 */
public class Organism implements Comparable<Organism> {		
	private static final int RANDOM_SEED = 11;
	public static final int DEFAULT_DNA_LENGTH = 10;
	
	private Random rng;
	private double[] dna;
	private int lengthDNAStrings;
	
	private double fitness;
	
	public Organism(){
		rng = new Random(Organism.RANDOM_SEED);
		lengthDNAStrings = 10;
		dna = new double[]{0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0};
		
		fitness = -1.0;
	}
	
	public Organism(Organism org){
		this.rng = org.getRNG();
		this.dna = org.getDNA();
		this.lengthDNAStrings = this.dna.length;
		
		fitness = org.getFitness();
	}
	
	public Organism(int lengthDNAStrings){
		rng = new Random(Organism.RANDOM_SEED);
		dna = new double[lengthDNAStrings];
		this.lengthDNAStrings = lengthDNAStrings;
		
		fitness = -1.0;
	}
	
	public Organism(int lengthDNAStrings, long seed){
		rng = new Random(seed);
		dna = new double[lengthDNAStrings];
		this.lengthDNAStrings = lengthDNAStrings;
		
		fitness = -1.0;
	}
	
	/** Getters **/
	public double[] getDNA(){ return dna; }
	public int getDNAStringLength(){ return lengthDNAStrings; }
	public Random getRNG() { return rng; }
	public double getFitness() { return fitness; }
	
	/** Setters **/
	public void setDNA(double[] dna){ this.dna = dna; }
	public void setFitness(double fitness) { this.fitness = fitness; }
	public void setRNG(long seed) { rng = new Random(seed); }
	
	// toString-function	
	public String toString(){
		return ("{Organism:[dna:" + Arrays.toString(dna) + ", fitness:" + fitness + "]}");
	}
	
	/**
	 * Helper-function that randomly initializes the DNA of an Organism
	 */
	public void initDNARandomly(){
		for(int i = 0; i < lengthDNAStrings; i++){
			dna[i] = rng.nextDouble();
		}
	}
	
	/**
	 * Helper-function that randomly initializes the DNA of an Organism, between given bounds
	 * @param lowerBound
	 * @param upperBound
	 */
	public void initDNARandomly(double lowerBound, double upperBound){
		for(int i = 0; i < lengthDNAStrings; i++){
			dna[i] = lowerBound + (upperBound - lowerBound) * rng.nextDouble();
		}
	}

	/**
	 * Function that allows an Array-object of Organisms to be sorted based on fitness
	 */
	@Override
	public int compareTo(Organism org) {
		return ((Double) fitness).compareTo(org.getFitness());
	}
}
